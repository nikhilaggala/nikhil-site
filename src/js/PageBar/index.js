import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import moonIcon from '../../static/svgs/moon.svg';
import sunIcon from '../../static/svgs/sun1.svg';

import classes from './index.css';

export default class PageBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      theme: 'dark',
    };
  }

  changeTheme = () => {
    const { theme } = this.state;

    if (theme === 'light') {
      return this.setState({
        theme: 'dark'
      }, () => {
        document.documentElement.setAttribute('data-theme', 'dark');
      });
    }

    return this.setState({
      theme: 'light'
    }, () => {
      document.documentElement.setAttribute('data-theme', 'light');
    });
  }

  renderThemeToggle = () => {
    const { theme } = this.state;
    const src = theme === 'light' ? moonIcon : sunIcon;

    return (
      <img className={classes.themeIcon} src={src} alt="theme" onClick={this.changeTheme} />
    );
  }

  onClickTab = (index) => {
    return () => this.setState({
      activeTabIndex: index
    });
  }

  renderTab = ({ item, index }) => {
    const { activeTabIndex } = this.state;

    const linkClassName = activeTabIndex === index
      ? classes.activeTab
      : classes.inactiveTab;

    return (
      <div className={classes.eachTab}>
        <Link
          to={item.to}
          className={linkClassName}
          onClick={this.onClickTab(index)}>
            {item.name}
        </Link>
      </div>
    );
  }

  render() {
    return (
      <div className={classes.container}>
        {this.renderThemeToggle()}
      </div>
    );
  }
}
