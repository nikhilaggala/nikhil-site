import React, { Component } from 'react';

import MailComponent from '../../Components/MailComponent';

import mailIcon from 'static/images/mial.png';
import instagramIcon from 'static/images/insta.png';
import gitlabIcon from 'static/images/gitlab.png';
import linkedinIcon from 'static/images/linkedin.png';
import classes from './index.css';

export default class ContactPage extends Component {
  renderAboutMe = () => {

  }

  renderContent = () => {
    return (
      <div className={classes.content}>
        {this.renderAboutMe()}
      </div>
    );
  }

  render() {
    const headerText = 'I\'m always happy to hear from you.';
    return (
      <div className={classes.pageContainer}>
        <span className={classes.headerText}>{headerText}</span>
        <MailComponent
          className={classes.mailContainer}
          iconClass={classes.mailIconClass}
          href={'mailto:nikhilaggala@gmail.com'}
          text={'nikhilaggala@gmail.com'}
          leftIcon={mailIcon}
          iconAltProp="mail" />
        <MailComponent
          className={classes.mailContainer}
          href={'https://www.linkedin.com/in/nikhil-aggala-69b9a7136/'}
          text={'LinkedIn'}
          leftIcon={linkedinIcon}
          iconAltProp="LinkedIn" />
        <MailComponent
          className={classes.mailContainer}
          href={'https://gitlab.com/nikhilaggala'}
          text={'GitLab'}
          leftIcon={gitlabIcon}
          iconAltProp="GitLab" />
        <MailComponent
          className={classes.mailContainer}
          href={'https://www.instagram.com/mr.aggala/'}
          text={'Instagram'}
          leftIcon={instagramIcon}
          iconAltProp="Instagram" />
      </div>
    );
  }
}
