import React, { Component } from 'react';

import myImage from '../../../static/images/mine2.jpeg';
import AppLink from '../../Components/AppLink';
import classes from './index.css';

export default class HomePage extends Component {
  renderImage = () => {
    return (
      <div className={classes.imageContainer}>
        <img
          src={myImage}
          alt="Nikhil Aggala"
          title="Nikhil Aggala"
          className={classes.myImage}
        />
      </div>
    );
  }

  renderAboutMe = () => {
    const headerText = 'Hello, Nice to meet you!';
    // const name = 'Nikihl Aggala'
    const text1 = 'I am ';
    const text2 = 'Nikhil Aggala';
    const text3 = ', currently available for New Opportunities.  ';
    // const text4 = 'I am developer by day and a fitness enthusiast by night.'
    const text4 = 'I\'ve got 3 + years of experience in designing and developing responsive user interfaces.';
    const text5 = 'I am developer by day and a fitness enthusiast by night. ';
    const text6 = 'Nothing interests me more until unless it appears challenging to me.';

    return (
      <div className={classes.description}>
        <span className={classes.welcomeText}>{headerText}</span>
        <div className={classes.selfText} >
          <p>
            <span>{text1}</span>
            <span className={classes.name}>{text2}</span>
            <span>{text3}</span>
          </p>
          <p>
            <span>{text4}</span>
          </p>
          <p className={classes.para2}>
            <span>{text5}</span>
            <span>{text6}</span>
          </p>
          <div className={classes.linkContainer}>
            <AppLink
              href="https://drive.google.com/file/d/1eBrtaZ1mlAjCaXwgermI0ws86m_i7-zT/view?usp=sharing"
              useAnchor
              target="_blank">
              <span className={classes.link} >Know more</span>
            </AppLink>
          </div>
        </div>
      </div>
    );
  }

  renderContent = () => {
    return (
      <div className={classes.content}>
        {this.renderImage()}
        {this.renderAboutMe()}
      </div>
    );
  }

  render() {
    return (
      <div className={classes.pageContainer}>
        {this.renderContent()}
      </div>
    );
  }
}
