import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import classes from './index.css';

export default class AppLink extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    highlightClass: PropTypes.string,
    href: PropTypes.string,
    useAnchor: PropTypes.bool,
    target: PropTypes.string,
    children: PropTypes.node,
    onClick: PropTypes.func
  }

  static defaultProps = {
    useAnchor: false,
  }

  handleClick = (e) => {
    const { onClick } = this.props;

    if (onClick) onClick(e);
  }

  renderAnchor = () => {
    const { href, target, children, className, highlightClass } = this.props;

    return (
      <div className={classes.linkContainer}>
        <a
          href={href}
          target={target}
          className={classnames(classes.link, className)}>
          {children}
        </a>
        <div className={classnames(classes.linkHighlight, highlightClass)} />
      </div>
    );
  }

  renderLink = () => {
    const { href, target, children, className, highlightClass } = this.props;

    return (
      <Link to={href}>
        <a target={target} className={classnames(classes.link, className)}>
          <div className={classes.linkContainer}>
            {children}
            <div className={classnames(classes.linkHighlight, highlightClass)} />
          </div>
        </a>
      </Link>
    );
  }

  render() {
    const { onClick, useAnchor } = this.props;

    if (!onClick && !useAnchor) return this.renderLink();

    if (!onClick && useAnchor) return this.renderAnchor();

    return (
      <button type="button" onClick={this.handleClick}>
        {!useAnchor && this.renderLink()}
        {useAnchor && this.renderAnchor()}
      </button>
    );
  }
}
