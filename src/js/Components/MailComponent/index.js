import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import AppLink from '../../Components/AppLink';
import classes from './index.css';


const MailComponent = props => {
  const { className, iconClass, text, href, iconAltProp, leftIcon } = props;

  function renderLeftIcon() {
    if (!leftIcon) return null;

    return (
      <img className={classnames(classes.icon, iconClass)} src={leftIcon} alt={iconAltProp} />
    );
  }

  return (
    <div className={classnames(classes.container, className)}>
      {renderLeftIcon()}
      <AppLink href={href} target="_blank" useAnchor>
        <span>
          {text}
        </span>
      </AppLink>
    </div>
  );
};

MailComponent.defaultProps = {
  iconAltProp: 'image'
};

MailComponent.propTypes = {
  className: PropTypes.string,
  iconClass: PropTypes.string,
  leftIcon: PropTypes.string,
  iconAltProp: PropTypes.string,
  href: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default MailComponent;
