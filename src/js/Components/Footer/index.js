import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import classes from './index.css';

const Footer = (props) => {
  const {
    items,
    className,
    itemClassName,
    renderItem,
    keyExtractor,
  } = props;

  const renderItems = () => {
    if (items.length < 1) return null;

    return items.map((item, index) => {
      return (
        <div
          key={keyExtractor({ item, index })}
          className={itemClassName}>
          {renderItem({ item, index })}
        </div>
      );
    });
  };

    return(
      <div className={classnames(classes.container, className)}>
        {renderItems()}
      </div>
    );
};

Footer.propTypes = {
  className: PropTypes.string,
  itemClassName: PropTypes.string,
  items: PropTypes.array,
  renderItem: PropTypes.func,
  keyExtractor: PropTypes.func,
};

export default Footer;
