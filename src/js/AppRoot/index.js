import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import { Provider } from 'react-redux';
// import { createBrowserHistory } from 'history';

import { routes } from '../routes';

import Footer from '../Components/Footer';

import classes from './index.css';

// const history = createBrowserHistory();

const items = [
  {
    id: 'about',
    slug: 'about',
    name: 'About',
    href: '/',
    hash: '#/'
  },
  // {
  //   id: 'projects',
  //   slug: 'projects',
  //   name: 'Projects',
  // },
  {
    id: 'contact',
    slug: 'contact',
    name: 'Contact',
    href: '/contact',
    hash: '#/contact'
  },
];

export default class AppRoot extends Component {
  state = {
    activeHash: window.location.hash
  }

  keyExtractor = ({ item, index }) => {
    return item.id || index;
  }

  returnActiveRoute = (hash) => {
    if (!window) return;

    this.setState({
      activeHash: hash
    });

    // if (window.location.hash === hash) return true;

    // return false;
  }

  renderItem = ({ item, index }) => {

    const itemTextClass = this.state.activeHash === item.hash ? classes.activeItemText : classes.inActiveItemText;
    const dotClass = this.state.activeHash === item.hash ? classes.activeDot : classes.inActiveDot;

    return (
      <div onClick={() => this.returnActiveRoute(item.hash)} className={classes.item} key={index}>
        <Link className={classes.anchor} to={item.href}>
          <span className={itemTextClass}> {item.name} </span>
          <div className={dotClass} />
        </Link>
      </div>
    );
  }

  render() {
    // const { routes, store } = this.props;
    return (
      // <Provider store={store}>
        <div className={classes.app}>
          <Switch>
            {
              routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route} />
              ))
            }
          </Switch>
          <Footer
            className={classes.footer}
            items={items}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />
        </div>
      // </Provider>
    );
  }
}

// For rendering sub routes
function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}
