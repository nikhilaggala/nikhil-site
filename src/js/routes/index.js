// Component imports
import HomePage from 'containers/HomePage';
import ContactPage from 'containers/ContactPage';

// Define all routes here
export const routes = [
  {
    exact: true,
    path: '/',
    component: HomePage
  },
  {
    path: '/about',
    component: HomePage,
  },
  {
    path: '/contact',
    component: ContactPage,
  }
  // example for sub routes
  // {
  //   path: "/home",
  //   component: Tacos,
  //   routes: [
  //     {
  //       path: "/tacos/bus",
  //       component: Bus
  //     },
  //     {
  //       path: "/tacos/cart",
  //       component: Cart
  //     }
  //   ]
  // }
];
