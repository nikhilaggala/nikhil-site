
const path = require('path');

module.exports = {
  containers: path.resolve(__dirname, './src/js/containers'),
  cD: path.resolve(__dirname, './src/js/componentsDumb'),
};
