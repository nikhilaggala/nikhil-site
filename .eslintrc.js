module.exports = {
  "env": {
      "browser": true,
      "es6": true,
      "webextensions": true,
      "node": true,
  },
  "extends": [
      "plugin:react/recommended",
      "eslint:recommended"
  ],
  parser: "babel-eslint",
  "globals": {
      "Atomics": "readonly",
      "SharedArrayBuffer": "readonly"
  },
  "parserOptions": {
      "ecmaFeatures": {
          "jsx": true
      },
      "ecmaVersion": 2018,
      "sourceType": "module"
  },
  "plugins": [
      "react"
  ],
  "rules": {
      "semi": ["warn", "always"],
      "quotes": ["warn", "single", { "avoidEscape": true }],
      "no-unused-vars": 1,
      "no-constant-condition": 1,
      "no-fallthrough": 1,
      "react/react-in-jsx-scope": "off",
  }
};
